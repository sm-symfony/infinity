<?php

require __DIR__.'/vendor/autoload.php';

$client = new \GuzzleHttp\Client([
    'base_uri' => 'http://localhost:8000',
    'defaults' => [
        'exceptions' => false
    ]
]);

$faker = \Faker\Factory::create();

$data = array(
    'sapld' => $faker->buildingNumber,
    'hostname' => $faker->unique()->domainName,
    'loopback' => $faker->unique()->ipv4,
    'mac' => $faker->unique()->macAddress
);

// 1) Create a router resource
$response = $client->post('/api/routers', [
    'body' => json_encode($data)
]);

//$routerUrl = $response->getHeader('Location');

// 2) GET a router resource
//$response = $client->get($routerUrl[0]);

echo $response->getBody();

echo "\n\n";