<?php


namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RouterRepository")
 * @ORM\Table(name="router")
 * @UniqueEntity("hostname")
 * @UniqueEntity("loopback")
 */
class Router
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Sapld can not be blank.")
     */
    private $sapld;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank(message="Hostname can not be blank.")
     */
    private $hostname;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Assert\NotBlank(message="Loopback can not be blank.")
     */
    private $loopback;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message="Mac address can not be blank.")
     */
    private $mac;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deleted_at;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSapld()
    {
        return $this->sapld;
    }

    /**
     * @param string $sapld
     */
    public function setSapld($sapld)
    {
        $this->sapld = $sapld;
    }

    /**
     * @return string
     */
    public function getHostname()
    {
        return $this->hostname;
    }

    /**
     * @param string $hostname
     */
    public function setHostname($hostname)
    {
        $this->hostname = $hostname;
    }

    /**
     * @return string
     */
    public function getLoopback()
    {
        return $this->loopback;
    }

    /**
     * @param string $loopback
     */
    public function setLoopback($loopback)
    {
        $this->loopback = $loopback;
    }

    /**
     * @return string
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * @param string $mac
     */
    public function setMac($mac)
    {
        $this->mac = $mac;
    }

    /**
     * @return mixed
     */
    public function getDeletedAt()
    {
        return $this->deleted_at;
    }

    /**
     * @param mixed $deleted_at
     */
    public function setDeletedAt($deleted_at)
    {
        $this->deleted_at = $deleted_at;
    }
}