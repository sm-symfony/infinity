<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RouterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sapld')
            ->add('hostname')
            ->add('loopback')
            ->add('mac', null, [
                'label' => 'Mac Address'
            ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success btn-lg pull-left', 'formnovalidate' => 'true']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Router'
        ]);
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_router_form_type';
    }
}
