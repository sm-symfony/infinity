<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Router;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        // create 20 products! Bam!
        for ($i = 0; $i < 20; $i++) {
            $router = new Router();
            $router->setSapld($faker->unique()->buildingNumber);
            $router->setHostname($faker->unique()->domainName);
            $router->setLoopback($faker->unique()->ipv4);
            $router->setMac($faker->unique()->macAddress);
            $manager->persist($router);
        }

        $manager->flush();
    }
}