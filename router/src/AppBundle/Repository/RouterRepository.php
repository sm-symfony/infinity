<?php


namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class RouterRepository extends EntityRepository
{
    public function findAllRouters()
    {
        return $this->createQueryBuilder('router')
            ->where('router.deleted_at is NULL')
            ->getQuery()
            ->execute();
    }
}