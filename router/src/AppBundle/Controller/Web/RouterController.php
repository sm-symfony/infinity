<?php

namespace AppBundle\Controller\Web;

use AppBundle\Entity\Router;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RouterController
 * @Security("has_role('ROLE_USER')")
 */
class RouterController extends Controller
{
    /**
     * @Route("/routers", name="router_list")
     */
    public function listAction()
    {
        $routers = $this->getDoctrine()
            ->getRepository('AppBundle:Router')
            ->findAllRouters();

        return $this->render('router/list.html.twig', [
            'routers' => $routers
        ]);
    }

    /**
     * @Route("/router/new", name="router_new")
     */
    public function newAction(Request $request)
    {
        $router = new Router();
        $form =  $this->createForm('AppBundle\Form\RouterFormType', $router);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $router = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($router);
            $em->flush();
            $this->addFlash('success', 'Router Created!!');
            return $this->redirectToRoute('router_list');
        }
        return $this->render('router/new.html.twig', [
            'routerForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/router/{id}/show", name="router_show")
     */
    public function showAction(Router $router)
    {
        return $this->render('router/show.html.twig', [
            'router' => $router
        ]);
    }

    /**
     * @Route("/router/{id}/edit", name="router_edit")
     */
    public function editAction(Request $request, Router $router)
    {
        $form =  $this->createForm('AppBundle\Form\RouterFormType', $router);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $router = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($router);
            $em->flush();
            $this->addFlash('success', 'Router Updated!!');
            return $this->redirectToRoute('router_list');
        }
        return $this->render('router/edit.html.twig', [
            'routerForm' => $form->createView(),
            'router' => $router
        ]);
    }

    /**
     * @Route("/router/{id}/delete", name="router_delete")
     */
    public function deleteAction(Request $request, Router $router)
    {
        $em = $this->getDoctrine()->getManager();
        $router->setDeletedAt(new \DateTime('now'));
        $em->persist($router);
        $em->flush();
        $this->addFlash('success', 'Router Deleted!!');
        return $this->redirectToRoute('router_list');
    }
}