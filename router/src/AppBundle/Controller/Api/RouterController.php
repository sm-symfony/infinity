<?php

namespace AppBundle\Controller\Api;


use AppBundle\Entity\Router;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RouterController extends Controller
{
    /**
     * @Route("/api/routers", methods={"POST"})
     */
    public function newAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $router = new Router();
        $form = $this->createForm('AppBundle\Form\RouterFormType', $router);
        $form->submit($data);

        $em = $this->getDoctrine()->getManager();
        $em->persist($router);
        $em->flush();

        $data = $this->serializeRouter($router);
        $response = new JsonResponse($data, 201);
        $routerUrl = $this->generateUrl('api_routers_show', [
            'ip' => $router->getLoopback()
        ]);
        $response->headers->set('Location', $routerUrl);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/api/routers/{ip}", methods={"GET"}, name="api_routers_show")
     */
    public function showAction($ip)
    {
        $router = $this->getDoctrine()
            ->getRepository('AppBundle:Router')
            ->findOneBy(['loopback' => $ip]);

        if (!$router) {
            throw $this->createNotFoundException(sprintf(
                'No router found with ip "%s"',
                $ip
            ));
        }

        $data = $this->serializeRouter($router);

        return new JsonResponse($data);
    }

    /**
     * @Route("/api/routers", methods={"GET"})
     */
    public function listAction()
    {
        $routers = $this->getDoctrine()
            ->getRepository('AppBundle:Router')
            ->findAll();

        $data = ['routers' => []];
        foreach ($routers as $router) {
            $data['routers'][] = $this->serializeRouter($$router);
        }
        $response = new Response(json_encode($data), 200);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    private function serializeRouter(Router $router)
    {
        return array(
            'sapld' => $router->getSapld(),
            'hostname' => $router->getHostname(),
            'loopback' => $router->getLoopback(),
            'mac' => $router->getMac()
        );
    }
}
