<?php

header("Content-Type: image/png");
$im = @imagecreatetruecolor(600, 600)
or die("Cannot Initialize new GD image stream");

$black = imagecolorallocate($im, 0, 0, 0);
$white = imagecolorallocate($im, 255, 255, 255);
$gray = imagecolorallocate($im, 220, 220, 220);

imagefill($im, 0, 0, $white);

// POLYGON
$poly_points = array(150, 100, 450, 100, 550, 300, 450, 500, 150, 500, 50,  300);
imagepolygon ($im, $poly_points, 6, $black);

// CIRCLE
imageellipse ($im, 300, 300, 350, 350, $black);

// SQUARE
$square_points = array(150, 300, 300, 150, 450, 300, 300, 450);
imagepolygon ($im, $square_points, 4, $black);

imagepng($im);
imagedestroy($im);