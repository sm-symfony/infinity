<?php

header("Content-Type: image/png");
$im = @imagecreatetruecolor(600, 600)
or die("Cannot Initialize new GD image stream");

$black = imagecolorallocate($im, 0, 0, 0);
$white = imagecolorallocate($im, 255, 255, 255);
$gray = imagecolorallocate($im, 220, 220, 220);

imagefill($im, 0, 0, $white);

$poly_points = array(50, 200, 550, 200, 500, 380, 300, 450, 100, 380);
imagepolygon ($im, $poly_points, 5, $black);
imagefilledellipse ($im, 50, 200, 50, 50, $gray);
imagefilledellipse ($im, 550, 200, 50, 50, $gray);
imagefilledellipse ($im, 500, 380, 50, 50, $gray);
imagefilledellipse ($im, 300, 450, 50, 50, $gray);
imagefilledellipse ($im, 100, 380, 50, 50, $gray);

imagepng($im);
imagedestroy($im);