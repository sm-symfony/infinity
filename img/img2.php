<?php

header("Content-Type: image/png");
$im = @imagecreatetruecolor(600, 600)
or die("Cannot Initialize new GD image stream");

$black = imagecolorallocate($im, 0, 0, 0);
$white = imagecolorallocate($im, 255, 255, 255);
$gray = imagecolorallocate($im, 220, 220, 220);

imagefill($im, 0, 0, $white);

imagesetthickness($im, 5);
$triangle1_points = array(300, 150, 450, 400, 150, 400);
imagepolygon ($im, $triangle1_points, 3, $black);

$triangle2_points = array(300, 50, 400, 200, 200, 200);
imagefilledpolygon ($im, $triangle2_points, 3, $gray);

$triangle3_points = array(150, 300, 250, 450, 50, 450);
imagefilledpolygon ($im, $triangle3_points, 3, $gray);

$triangle4_points = array(450, 300, 350, 450, 550, 450);
imagefilledpolygon ($im, $triangle4_points, 3, $gray);

imagepng($im);
imagedestroy($im);