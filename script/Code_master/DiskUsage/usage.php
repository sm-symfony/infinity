<?php

/**
 * Disk Status Class - Example
 *
 * http://pmav.eu/stuff/php-disk-status/
 *
 * 22/Aug/2009
 */

require_once 'DiskStatus.php';

try {
	//$diskStatus = new DiskStatus('c:');
  $diskStatus = new DiskStatus('/');

  $freeSpace = $diskStatus->freeSpace();
  $totalSpace = $diskStatus->totalSpace();
  $barWidth = ($diskStatus->usedSpace()/100) * 400;

} catch (Exception $e) {
  echo 'Error ('.$e->getMessage().')';
  exit();
}

?>
    
Usage: <?= $diskStatus->usedSpace() ?>%
Free: <?= $freeSpace ?> (of <?= $totalSpace ?>)
  
      
