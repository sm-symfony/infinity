#!/bin/sh
# script to check server for extremely high load and restart Apache if the condition is matched
# Besides restarting the script copies a temporary index page to a main Apache DocumentRoot for few seconds to reduce possible load done by too many client requests
# Below whether you see 'cp -rpf' uncomment if you want to use the set temporary index page 
# Written by hip0
# http://www.pc-freak.net
check=`cat /proc/loadavg | sed 's/\./ /' | awk '{print $1}'`
# define max load avarage when script is triggered 
max_load='25'
# log file
high_load_log='/var/log/apache_high_load_restart.log';
# location of inidex.php to overwrite with temporary message
index_php_loc='/home/site/www/index.php';
# location to Apache init script
apache_init='/etc/init.d/apache2';
#
site_maintenance_msg='<html><head><title> Site maintenance, please excuse us</title></head><body><h2>Site Maintenance in progress - We will be back online in a minute</h2></body></html>';
# temporary pid file
pid='/tmp/apache_restart.pid';

# Create copy backup of original site index
##cp -rpf $index_php_loc.bak_ap index_php_loc.bak_ap.orig
if [ ! -f $pid ]; then
echo '1' >$pid

if [ $check -gt "$max_load" ]; then
	#25 is load average on 5 minutes
##	cp -rpf $index_php_loc $index_php_loc.bak_ap
	echo "$site_maintenance_msg" > $index_php_loc
	sleep 15;
	if [ $check -gt "$max_load" ]; then
	$apache_init stop
	sleep 5;
	$apache_init restart
	echo "$(date) : Apache Restart due to excessive load | $check |" >> $high_load_log;
##	cp -rpf $index_php_loc.bak_ap $index_php_loc
	fi

fi

fi
